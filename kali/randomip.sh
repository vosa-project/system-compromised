#!/bin/bash

HOST=$((1+$RANDOM%253))
NETWORK=$((1+$RANDOM%4))

if [[ $NETWORK -eq 1 ]]; then

	IP=192.168.66.$HOST

elif [[ $NETWORK -eq 2 ]]; then

	IP=10.10.10.$HOST

elif [[ $NETWORK -eq 3 ]]; then

	IP=192.168.0.$HOST

elif [[ $NETWORK -eq 4 ]]; then

	IP=10.66.10.$HOST

else

	echo "something went wrong"

fi

echo $IP

ifconfig eth0 $IP netmask 255.255.255.0

ifdown eth0 && ifup eth0
