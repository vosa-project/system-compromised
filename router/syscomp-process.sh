#!/bin/bash
# Script for checking  System Compromised lab objectives
# Objective name - killing nudoku process

# Author - Katrin Loodus
# Modified by  Roland Kaur
#
# Date - 17.11.2016
# Version - 0.0.1

LC_ALL=C

# START
# SYSCOMPROCESS

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/tmp/process"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops server
	IP_to_SSH=192.168.6.5

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=process

	# Next step uname in VirtualTA
        Nuname=removespam

        # Next step objective oname
        Noname=actuality

        # Call out next step if hidden
        Callout=true


		if $Callout ; then

                # Fetch next step json entry
                curl -H "Content-Type: application/json" -X GET -d '{"api_key":"'"$TA_KEY"'", "lab":"'"$LAB_ID"'", "uname":"'"$Nuname"'" }' $VIRTUALTA_HOSTNAME/api/v1/step > /tmp/nextstep

                # Parse next step json entries into variables
                STEP_INST=$(cat /tmp/nextstep | jq '.instruction') && TEMP="${STEP_INST%\"}" && STEP_INST="${TEMP#\"}"
                STEP_TITLE=$(cat /tmp/nextstep | jq '.title') && TEMP="${STEP_TITLE%\"}" && STEP_TITLE="${TEMP#\"}"
                STEP_LONGTITLE=$(cat /tmp/nextstep | jq '.longTitle') && TEMP="${STEP_LONGTITLE%\"}" && STEP_LONGTITLE="${TEMP#\"}"
                STEP_MATERIAL=$(cat /tmp/nextstep | jq '.material') && TEMP="${STEP_MATERIAL%\"}" && STEP_MATERIAL="${TEMP#\"}"
                STEP_WEB=$(cat /tmp/nextstep | jq '.web') && TEMP="${STEP_WEB%\"}" && STEP_WEB="${TEMP#\"}"
                STEP_COMPM=$(cat /tmp/nextstep | jq '.completionMethod') && TEMP="${STEP_COMPM%\"}" && STEP_COMPM="${TEMP#\"}"
                STEP_USRKEY=$(curl -H "Content-Type: application/json" -X GET -d '{"api_key":"'"$TA_KEY"'", "username":"'"$LAB_USERNAME"'"}' $VIRTUALTA_HOSTNAME/api/v1/user | jq '.key' | jq '.key') && TEMP="${STEP_USRKEY%\"}" && STEP_USRKEY="${TEMP#\"}"

		fi
}

# User interaction: Install apache and nginx

SYSCOMPROCESS () {

	while true
	do

   	# Check if user has installed apache and nginx 
    	ssh root@$IP_to_SSH 'ps -ef | grep "[/]usr/games/nudoku_scores"'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -ne 0 ]; then

        	echo -e "\nnudoku has been killed!! Date: `date`\n" && touch $CheckFile

		if $Callout ; then

                        # Post next step
                        curl -H "Content-Type: application/json" -X POST -d '{"api_key":"'"$TA_KEY"'", "lab": "'"$LAB_ID"'", "user":"'"$STEP_USRKEY"'", "oname":"'"$Noname"'", "after": "'"$Uname"'", "uname":"'"$Nuname"'", "enabled": true, "weight": 1, "title": "'"$STEP_TITLE"'" , "longTitle": "'"$STEP_LONGTITLE"'", "instruction": "'"$STEP_INST"'" , "material": "'"$STEP_MATERIAL"'" , "web": "'"$STEP_WEB"'", "completionMethod": "'"$STEP_COMPM"'"  }' $VIRTUALTA_HOSTNAME/api/v1/labuser_step

                fi

        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Nudoku has not been killed! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

SYSCOMPROCESS

exit 0
